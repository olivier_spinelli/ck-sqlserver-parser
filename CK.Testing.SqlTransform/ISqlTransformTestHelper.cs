using System;

namespace CK.Testing
{
    public interface ISqlTransformTestHelper : IMixinTestHelper, IMonitorTestHelper, SqlTransform.ISqlTransformTestHelperCore
    {
    }
}
